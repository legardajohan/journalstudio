$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });

    $('#modalReserva').on('show.bs.modal', function(e){
      console.log('El modal se está ejecutando');

      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-outline-warning');
      $('#contactoBtn').prop('disable', true);
    });
    $('#modalReserva').on('shown.bs.modal', function(e){
      console.log('El modal se ejecutó');
    });
    $('#modalReserva').on('hide.bs.modal', function(e){
      console.log('El modal se oculta');
    });
    $('#modalReserva').on('hidden.bs.modal', function(e){
      console.log('El modal se ocultó');
      $('#contactoBtn').removeClass('btn-outline-warning');
      $('#contactoBtn').addClass('btn-success');
      $('#contactoBtn').prop('disable', false);
    });

  });